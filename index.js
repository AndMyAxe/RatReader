var http = require('http')
var fs = require('fs')
var path = require('path')

var Epub = require('epub')
var htmlToText = require('html-to-text')

var ip = require('ip')
var ipAddress = ip.address()

fs.writeFile('ip_address.js', `var ipAddress = "${ipAddress}"`)

var booklist = {}

populateBooklist()

// Get list of available books
function populateBooklist () {
  fs.readdir('./Books', function (err, items) {
    if (err) {
      console.log(err)
    } else {
      // populate booklist with information
      items.forEach(function(item) {
        var extension = path.extname(item)
        if (extension === '.txt' || extension === '.epub') {
          booklist[item] = extension
        }
      })
    }
  })
}

// Simple server
http.createServer(function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Request-Method', '*')
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
  res.setHeader('Access-Control-Allow-Headers', '*')
  if (req.url === '/') {
    res.writeHead(200, {'Content-Type': 'text/html'})
    var content = fs.readFileSync('./index.html')
    res.end(content, 'utf-8')
  } else if (req.url === '/css') {
    res.writeHead(200, {'Content-Type': 'text/css'})
    var content = fs.readFileSync('./reader.css')
    res.end(content, 'utf-8')
  } else if (req.url === '/js') {
    res.writeHead(200, {'Content-Type': 'text/javascript'})
    var content = fs.readFileSync('./reader.js')
    res.end(content, 'utf-8')
  } else if (req.url === '/bookList') {
    // Load the booklist.
    res.writeHead(200, {'Content-Type': 'text/plain'})
    var text = JSON.stringify(booklist)
    res.end(text)
  } else if (req.url.startsWith('/load/')) {
    var urlparts = req.url.split('/')
    // Check if the book is txt or epub
    if (urlparts[2].endsWith('.epub')) {
      // Load book from the booklist
      var epub = new Epub (`./Books/${urlparts[2]}`)
      epub.on("end", function(err){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        epub.getChapter(epub.flow[Number(urlparts[3])].id, function (err, text) {
          res.end(JSON.stringify({text: htmlToText.fromString(text), total: Object.keys(epub.flow).length}))
        })
      })
      epub.parse()
    } else if (urlparts[2].endsWith('.txt')) {
      res.writeHead(200, {'Content-Type': 'text/plain'})
      fs.readFile(`./Books/${decodeURI(urlparts[2])}`, 'utf8', function(err, data) {
        if (err) {
          console.log(err)
        } else {
          res.end(JSON.stringify({text: data, total: 1}))
        }
      })
    }
  } else if (req.url.startsWith('/settings')) {
    var urlparts = req.url.split('/')
    if (!urlparts[2]) {
      // Send back list of people
      var settings = require('./Settings/Settings.json')
      var people = settings.people;
      res.writeHead(200, {'Content-Type': 'text/plain'})
      res.end(JSON.stringify({people: people}))
    } else {
      // Send back settings for the selected person
      var personSettings = require(`./Settings/${urlparts[2]}.json`)
      res.writeHead(200, {'Content-Type': 'text/plain'})
      res.end(JSON.stringify(personSettings))
    }
  } else if (req.url === '/ip') {
    var content = fs.readFileSync('./ip_address.js')
    res.end(content, 'utf-8')
  } else if (req.url.startsWith('/bookmark')) {
    var urlparts = req.url.slice('10').split("&")
    var person = urlparts[0].split("=")[1]
    var book = urlparts[1].split("=")[1]
    var chapter = urlparts[2].split("=")[1]
    var paragraph = urlparts[3].split("=")[1]
    var word = urlparts[4].split("=")[1]
    var fontsize = urlparts[5].split("=")[1].slice(0,-2)
    var wpm = urlparts[6].split("=")[1]
    var settings = require(`./Settings/${person}.json`)
    settings = settings || {}
    settings.Bookmarks = settings.Bookmarks || {}
    settings.Bookmarks[book] = settings.Bookmarks[book] || {}
    settings.Bookmarks[book].word = word
    settings.Bookmarks[book].paragraph = paragraph
    settings.Bookmarks[book].chapter = chapter
    settings['font-size'] = fontsize
    settings.WPM = wpm
    settings.CurrentBook = book
    fs.writeFile(`./Settings/${person}.json`, JSON.stringify(settings, null, 2))
    res.end()
  }
}).listen(8080)
console.log('The RatReader is now being served on localhost:8080')
