var currentParagraph = 0,
  currentIndex = 0,
  centerLetter = '',
  stop = true,
  processID = null,
  previousWord = '',
  nextWord = '',
  text = '',
  words = [],
  paragraphs = [],
  numParagraphs = 0,
  baseDuration = 100,
  local = true,
  currentChapter = 0,
  chapter = '',
  bookName = '',
  loaded = false,
  speaking = false,
  currentPerson = '',
  TotalChapters = 0,
  rawText = '',
  speakingCharacterIndex = 0

var TextContainerElement = document.getElementById('TextContainer')
var TotalParagraphsSpan = document.getElementById('TotalParagraphs')

var synth = window.speechSynthesis
var voices = synth.getVoices()
var utterance = new SpeechSynthesisUtterance()
utterance.rate = 1

populateVoices()
addKeyboardShortcuts()
// do this with a callback so that it acutally works correctly
loadPersonList(loadServerList)

function showBulkWords () {
  // Hide the serial words

  var serialContainer = document.getElementById("TextContainer")
  console.log(serialContainer)
  serialContainer.style.display = 'none'
  var bodyThing = document.getElementsByTagName("body")
  console.log(bodyThing)
  bodyThing.overflow = 'scroll'

  // Put text into the bulk container
  var bulkContainer = document.getElementById("BulkWords")
  bulkContainer.innerHTML = `<p>${rawText}</p>`
}

function saveBookmark () {
  var fontsize = document.getElementById("TextContainer").style.fontSize
  var wpm = document.getElementById('newWPM').value
  currentPerson = document.getElementById('PersonSelect').value
  var queryString = `?person=${currentPerson}&book=${bookName}&chapter=${currentChapter}&paragraph=${currentParagraph}&word=${currentIndex}&fontsize=${fontsize}&wpm=${wpm}`
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `http://${ipAddress}:8080/bookmark${queryString}`, true);
  httpRequest.send()
}

function addKeyboardShortcuts () {
  document.addEventListener('keydown', function (event) {
    if (event.keyCode === 32) {
      // spacebar
      playPause()
    } else if (event.keyCode === 39) {
      // right
      showNextWord()
    } else if (event.keyCode === 37) {
      // left
      showPreviousWord()
    } else if (event.keyCode === 38) {
      // up
      loadNextParagraph()
    } else if (event.keyCode === 40) {
      // down
      loadPreviousParagraph()
    }
  })
}

/*
  This populates the list of voices
*/
function populateVoices () {
  for(i = 0; i < voices.length ; i++) {
    var option = document.createElement('option');
    option.textContent = voices[i].name + ' (' + voices[i].lang + ')';
    if(voices[i].default) {
      option.textContent += ' -- DEFAULT';
    }
    option.setAttribute('data-lang', voices[i].lang);
    option.setAttribute('data-name', voices[i].name);
    document.getElementById("VoiceSelect").appendChild(option);
  }
}

function setUtterance () {
  var voiceSelect = document.getElementById("VoiceSelect")
  var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');
  for(i = 0; i < voices.length ; i++) {
    if(voices[i].name === selectedOption) {
      utterance.voice = voices[i];
    }
  }
}

function loadPersonList(cb) {
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `http://${ipAddress}:8080/settings`, true);
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      makePersonSelect(httpRequest.responseText)
      if (typeof cb === 'function') {
        cb()
      }
    }
  }
  httpRequest.send();
}

function makePersonSelect (response) {
  var people = JSON.parse(response)
  var options = ''
  people.people.forEach(function (person) {
    options += `<option value='${person}'>${person}</option>`
  })
  var selections = document.getElementById('PersonSelect')
  selections.innerHTML = options
}

function loadPersonSettings () {
  currentPerson = document.getElementById('PersonSelect').value
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `http://${ipAddress}:8080/settings/${currentPerson}`, true);
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      updateSettings(JSON.parse(httpRequest.responseText))
    }
  }
  httpRequest.send();
}

function updateSettings (settings) {
  if (settings['font-size']) {
    document.getElementById('newSize').value = settings['font-size']
    document.getElementById("TextContainer").style.fontSize = settings['font-size'] + 'px';
  }
  if (settings['background-color']) {
    document.getElementsByTagName('body')[0].style.backgroundColor = settings['background-color']
  }
  if (settings['font-family']) {
    document.getElementById('TextContainer').style.fontFamily = settings['font-family']
  }
  if (settings.WPM) {
    document.getElementById('newWPM').value = settings.WPM
    // Convert wpm to milliseconds per word
    baseDuration = Number(1000/settings.WPM*60)
  }
  if (settings.border) {
    document.getElementById('TextContainer').style.border = settings.border
  }
  if (settings.centerLetter) {
    if (settings.centerLetter.color) {
      document.getElementById('Center').style.color = settings.centerLetter.color
    }
  }
  if (settings.otherWords) {
    if (settings.otherWords.color) {
      document.getElementById('PreviousWord').style.color = settings.otherWords.color
      document.getElementById('NextWord').style.color = settings.otherWords.color
    }
  }
  if (settings.currentWord) {
    if (settings.currentWord.color) {
      document.getElementById('FirstHalf').style.color = settings.currentWord.color
      document.getElementById('LastHalf').style.color = settings.currentWord.color
    }
  }
  if (settings.CurrentBook) {
    bookName = settings.CurrentBook
    loadServerText(bookName)
    showCurrentWord()
  }
  if (settings.Bookmarks) {
    if (settings.Bookmarks[bookName]) {
      currentChapter = Number(settings.Bookmarks[bookName].chapter-1) || 0
      currentIndex = Number(settings.Bookmarks[bookName].word) || 0
      currentParagraph = Number(settings.Bookmarks[bookName].paragraph) || 0
      loadNextChapter(currentParagraph, currentIndex)
    }
  }
}

/*
  Load list of books on the server
*/
function loadServerList () {
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `http://${ipAddress}:8080/bookList`, true);
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      makeBookSelect(httpRequest.responseText)
    }
  }
  httpRequest.send();
}

function makeBookSelect (response) {
  var books = JSON.parse(response)
  var options = ''
  Object.keys(books).forEach(function (book) {
    options += `<option value='${book}'>${book}</option>`
  })
  var selections = document.getElementById('BookSelect')
  selections.innerHTML = options
}

/*
  Load text from the server.
*/
function loadServerText (book) {
  local = false
  bookName = book || document.getElementById('BookSelect').value
  if (bookName) {
    httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', `http://${ipAddress}:8080/load/${bookName}/0`, true);
    httpRequest.onreadystatechange = function(){
      // Process the server response here.
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        // Everything is good, the response was received.
        rawText = JSON.parse(httpRequest.responseText).text
        paragraphs = rawText.split(/(?:\r?\n){2,}/)
        words = paragraphs[currentParagraph].split(/\s|\n/)
        words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
        TotalChapters = JSON.parse(httpRequest.responseText).total
        updateChapterDisplay()
        updateParagarphListing()
        showCurrentWord()
        loaded = true
      }
    }
    httpRequest.send();
  }
}

function updateChapterDisplay () {
  document.getElementById('TotalChapters').innerHTML = TotalChapters
  document.getElementById('CurrentChapter').innerHTML = currentChapter + 1
}

/*
  Load text from a local file.
*/
function loadText () {
  local = true
  var reader = new FileReader()
  var fileInput = document.getElementById('book_text')
  var file = fileInput.files[0]
  bookName = file.name
  reader.onload = function (e) {
    text = reader.result
    // We need both pieces of the regex here because windows is weird
    paragraphs = text.split(/(?:\r?\n){2,}/)
    updateParagarphListing()
    words = paragraphs[currentParagraph].split(/\s|\n/)
    words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
    loaded = true
    showCurrentWord()
  }
  reader.readAsText(file)
}

function updateParagarphListing () {
  TotalParagraphsSpan.innerHTML = paragraphs.length
  document.getElementById('CurrentParagraph').innerHTML = currentParagraph + 1
}

/*
  This updates the delay between words based on the input words per minute
*/
function updateDelay () {
  var thing = document.getElementById('newWPM')
  // Convert wpm to milliseconds per word
  baseDuration = Number(1000/thing.value*60)
}

/*
  This function updates the words displayed in the reader section of the screen
*/
function updateDisplay (parts, previousWord, nextWord) {
  var PreviousContainer = document.getElementById('PreviousWord')
  var NextContainer = document.getElementById('NextWord')
  var FirstHalfContainer = document.getElementById('FirstHalf')
  var CenterContainer = document.getElementById('Center')
  var LastHalfContainer = document.getElementById('LastHalf')
  PreviousContainer.innerHTML = previousWord
  NextContainer.innerHTML = nextWord
  FirstHalfContainer.innerHTML = parts.firstBit
  CenterContainer.innerHTML = parts.centerLetter
  LastHalfContainer.innerHTML = parts.lastBit
}

/*
  This function handles changes to the font size
*/
function updateFontSize () {
  var thing = document.getElementById('newSize')
  document.getElementById("TextContainer").style.fontSize = thing.value + 'px';
}

function showCurrentWord () {
  var previousWord = currentIndex > 0 && words[currentIndex-1]? words[currentIndex-1]:''
  var nextWord = currentIndex < words.length && words[currentIndex+1]? words[currentIndex+1]:''
  var parts = splitWord(words[currentIndex])
  updateDisplay(parts, previousWord, nextWord)
  return parts.delay
}

/*
  This function shows the next word by preparing the word and then updating the
  dispaly as needed.
*/
function showNextWord () {
  if (loaded) {
    if (currentIndex < words.length-1) {
      currentIndex += 1
      var delay = showCurrentWord()
      if (!stop) {
        if (!speaking) {
          clearTimeout(processID)
          processID = setTimeout(showNextWord, delay)
        }
      }
    } else {
      loadNextParagraph()
      if (currentParagraph < paragraphs.length) {
        if (!stop) {
          if (!speaking) {
            clearTimeout(processID)
            var delay = 5*baseDuration > 500 ? 500 : 5*baseDuration
            processID = setTimeout(showNextWord, delay)
          }
        }
      }
    }
  }
}

/*
  This displays the next word when the text is being spoken.
*/
function speakNextWord () {
  if (loaded) {
    if (currentIndex < words.length-1) {
      showCurrentWord()
      currentIndex += 1
    } else {
      showCurrentWord()
      currentIndex = 0
      loadNextParagraph()
    }
  }
}

/*
  This moves the current text position back by one word.
*/
function showPreviousWord() {
  if (loaded) {
    if (currentIndex > 0) {
      currentIndex -= 1
      showCurrentWord()
    } else {
      previousParagraph()
    }
  }
}

/*
  This moves the current text position forward by one paragraph.
*/
function nextParagraph () {
  if (currentParagraph < numParagraphs && loaded) {
    stop = true
    loadNextParagraph()
  } else {
    loadNextParagraph()
  }
}

/*
  This moves the current text position back by one paragraph.
*/
function previousParagraph () {
  if (currentParagraph > 0 && loaded) {
    stop = true
    loadPreviousParagraph()
  } else {
    loadPreviousChapter()
  }
}

/*
  This loads the previous paragraph
*/
function loadPreviousParagraph () {
  if (loaded) {
    if (currentParagraph > 0) {
      var CurrentParagraphSpan = document.getElementById('CurrentParagraph')
      currentParagraph -= 1
      CurrentParagraphSpan.innerHTML = currentParagraph + 1
      words = paragraphs[currentParagraph].split(/\s|\n/)
      words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
      currentIndex = words.length-2
      showNextWord()
    } else if (currentChapter > 0) {
      loadPreviousChapter()
    }
  }
}

function boundaryListener () {
    //words = paragraphs[currentParagraph].split(/\s|\n/)
    //words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
    currentWordThing = /[^\s|\n]+/.exec(paragraphs[currentParagraph].slice(event.charIndex))
    if (words.indexOf(currentWordThing[0]) !== -1) {
      speakingCharacterIndex = event.charIndex
      currentIndex = words.indexOf(currentWordThing[0])
      speakNextWord()
    }
}

/*
  This loads the next paragaph
*/
function loadNextParagraph () {
  if (loaded) {
    if (currentParagraph < paragraphs.length-1) {
      var CurrentParagraphSpan = document.getElementById('CurrentParagraph')
      currentParagraph += 1
      CurrentParagraphSpan.innerHTML = currentParagraph + 1
      words = paragraphs[currentParagraph].split(/\s|\n/)
      words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
      currentIndex = -1
      if (speaking) {
        utterance.removeEventListener('boundary', boundaryListener)
        utterance.text = paragraphs[currentParagraph]
        utterance.onboundary = boundaryListener
        if (!stop) {
          synth.cancel()
          synth.speak(utterance)
        }
      } else {
        showNextWord()
      }
    } else if (local === false) {
      loadNextChapter()
    }
  }
}

/*
  Loads the next chapter from the server.
*/
function loadNextChapter (paragraphIndex, wordIndex) {
  if (bookName) {
    if (currentChapter < document.getElementById('TotalChapters').innerHTML-1 || (paragraphIndex && wordIndex)) {
      currentChapter += 1
      httpRequest = new XMLHttpRequest();
      httpRequest.open('GET', `http://${ipAddress}:8080/load/${bookName}/${currentChapter}`, true);
      httpRequest.onreadystatechange = function(){
        // Process the server response here.
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          // Everything is good, the response was received.
          TotalChapters = JSON.parse(httpRequest.responseText).total
          rawText = JSON.parse(httpRequest.responseText).text
          paragraphs = rawText.split(/(?:\r?\n){2,}/)
          //paragraphs = JSON.parse(httpRequest.responseText).text.split(/(?:\r?\n){2,}/)
          currentParagraph = paragraphIndex || 0
          currentIndex = wordIndex || 0
          words = paragraphs[currentParagraph].split(/\s|\n/)
          words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
          updateChapterDisplay()
          updateParagarphListing()
          showCurrentWord()
          loaded = true
          local = false
          if (!stop) {
            if (speaking) {
              speakNextWord()
            } else {
              var delay = 5*baseDuration > 500 ? 500 : 5*baseDuration
              processID = setTimeout(showNextWord, delay)
            }
          }
        }
      }
      httpRequest.send();
    }
  }
}

function loadPreviousChapter () {
  if (bookName) {
    if (currentChapter > 0) {
      currentChapter -= 1
      httpRequest = new XMLHttpRequest();
      httpRequest.open('GET', `http://${ipAddress}:8080/load/${bookName}/${currentChapter}`, true);
      httpRequest.onreadystatechange = function(){
        // Process the server response here.
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          // Everything is good, the response was received.
          paragraphs = JSON.parse(httpRequest.responseText).text.split(/(?:\r?\n){2,}/)
          numParagraphs = paragraphs.length
          TotalParagraphsSpan.innerHTML = numParagraphs
          currentParagraph = 0
          currentIndex = 0
          document.getElementById('CurrentParagraph').innerHTML = currentParagraph + 1
          words = paragraphs[currentParagraph].split(/\s|\n/)
          words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
          updateChapterDisplay()
          updateParagarphListing()
          showCurrentWord()
        }
      }
      httpRequest.send();
    }
  }
}

/*
  This prepares the current word for display.
*/
function splitWord (word) {
  var first, last, center, delay, wordLength
  wordLength = word.length

  if (wordLength > 2) {
    timeOut = wordLength > 10 ? 2*baseDuration : baseDuration+0.1*baseDuration*wordLength
  } else {
    timeOut = 1.5*baseDuration
  }

  timeOut = wordLength > 2 ? baseDuration+0.1*baseDuration*wordLength : 1.5*baseDuration

  if (wordLength > 1) {
    // if any of the things in this array are in a word than the reader stays
    // on that word for some extra time.
    var delayArray = ['\\?', '\\:', '\\,', '\\.', '\\;']
    var delayThing = new RegExp(delayArray.join('|')).test(word)

    timeOut = delayThing ? 1.25*timeOut : timeOut

    // If a word has a dash in it than the dash is the center.
    // This seems to work well.
    var dashIndex = word.indexOf('-')
    if (dashIndex > 0) {
      parts = word.split('-')
      first = parts[0]
      last = parts[1]
      center = '-'
    } else {
      var len1 = Math.floor(word.length/2)
      var len2 = word.length - len1
      first = len1 > 0 ? word.substring(0,len1-1):''
      last = len2 > 0 ? word.substring(len1):''
      if (len2 === 0) {
        center = word.substring(-1)
      } else {
        center = word.substring(len1-1,len1)
      }
    }
  } else {
    first = ''
    last = ''
    center = word
  }
  return {firstBit: first, lastBit: last, centerLetter: center, delay: timeOut}
}

/*
  If the reader is playing than stop it, otherwise start it.
*/
function playPause() {
  if (loaded) {
    if (speaking) {
      if (stop) {
        if (synth.pending) {
          synth.cancel()
        }
        stop = false
        utterance.removeEventListener('boundary', boundaryListener)
        var currentText = paragraphs[currentParagraph].slice(speakingCharacterIndex)
        words = currentText.split(/\s|\n/)
        words = words.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
        utterance.text = currentText
        utterance.onboundary = boundaryListener
        synth.speak(utterance)
      } else {
        stop = true
        synth.cancel()
      }
    } else {
      clearTimeout(processID)
      if (stop) {
        stop = false
        showNextWord()
      } else {
        stop = true
      }
    }
  }
}

/*
  Toggle speech
*/
function toggleSpeech () {
  var speechButton = document.getElementById('ToggleSpeech')
  if (speaking) {
    speaking = false
    speechButton.value='Speech: OFF'
  } else {
    speaking = true
    speechButton.value='Speech: ON'
  }
}
